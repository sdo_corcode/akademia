(function () {
    var app = angular.module('myApp', ['pascalprecht.translate']);

    app.config(function ($translateProvider) {
        $translateProvider
            .translations('en', translations.en)
            .translations('pl', translations.pl);

        $translateProvider.preferredLanguage('en');
    });

    app.controller('TranslateController', ['$translate', function ($translate) {
        this.switchLanguage = function () {
            if ($translate.proposedLanguage() === 'en') {
                $translate.use('pl');
            } else {
                $translate.use('en');
            }
        };
    }]);

    app.controller('MenuController', [function () {
        var self = this;

        self.tab = 0;

        self.setTab = function (newValue) {
            self.tab = newValue === self.tab ? 0 : newValue;
        };

        self.isSet = function (tabName) {
            return tabName === self.tab;
        };

        self.showContactForm = false;

        self.toggleContactForm = function () {
            self.showContactForm = !self.showContactForm;
        };
    }]);

    app.controller('MessageController', [function () {
        var self = this;

        self.message = {};

        self.addMessage = function () {
            messages.push(self.message);
            self.message = {};
        };
    }]);

    app.controller('MessagesController', [function () {
        this.messages = messages;
    }]);

    var messages = [];

    var translations = {
        en: {
            "language": "Switch language / EN",
            "aboutCompany": "About company",
            "projects": "Projects",
            "products": "Products",
            "contactUs": "Contact us",

            "headerMessage": "To they four in love. Settling you has separate supplied bed. Concluded resembled suspected his resources curiosity joy. Led all cottage met enabled attempt through talking delight.Dare he feet my tell busy. Considered imprudence of he friendship boisterous.",
            "aboutCompanyDesc": "His exquisite sincerity education shameless ten earnestly breakfast add. So we me unknown as improve hastily sitting forming. Especially favourable compliment but thoroughly unreserved saw she themselves. Sufficient impossible him may ten insensible put continuing. Oppose exeter income simple few joy cousin but twenty. Scale began quiet up short wrong in in. Sportsmen shy forfeited engrossed mayan.",
            "projectsDesc": "Do am he horrible distance marriage so although. Afraid assure square so happen mr an before. His many same been well can high that. Forfeited did law eagerness allowance improving assurance bed. Had saw put seven joy short first. Pronounce so enjoyment my resembled in forfeited sportsman. Which vexed did began son abode short may. Interested astonished he at cultivated or me. Nor brought one invited she produce her.",
            "productsDesc": "There worse by an of miles civil. Manner before lively wholly am mr indeed expect. Among every merry his yet has her. You mistress get dashwood children off. Met whose marry under the merit. In it do continual consulted no listening. Devonshire sir sex motionless travelling six themselves. So colonel as greatly shewing herself observe ashamed. Demands minutes regular ye to detract is.",

            "contactForm": "Contact Form",
            "nameForm": "Name",
            "messageForm": "Message",
            "sendForm": "Send",
            "placeholderNameForm": "First & Last Name",
            "placeholderEmailForm": "example@domain.com",

            "messages": "Messages",
            "messagesFrom": "From",
            "messagesMessage": "Message"
        },
        pl: {
            "language": "Zmień język / PL",
            "aboutCompany": "O firmie",
            "projects": "Projekty",
            "products": "Produkty",
            "contactUs": "Kontakt",

            "headerMessage": "Aby oni cztery w miłości. Usadawianie cię ma oddzielne dostarczone łóżko. Zakończony być podobnym podejrzewać jego zasoby radość ciekawości. Sprawiony wszyscy, kto domek spotkał umożliwiona próba przez rozmawianie o radości. Ośmielać się on stopy mój mówić zajęty. Rozważony brak rozwagi z on przyjaźń hałaśliwy.",
            "aboutCompanyDesc": "Jego nieskazitelna szczerość edukacja bezwstydny dziesięć szczerze śniadanie dodawać. Tak my mnie nieznany jak poprawiać się pośpiesznie zakładanie posiedzenia. Szczególnie przychylny komplement ale zupełnie niezarezerwowany zobaczył ona siebie. Wystarczający niemożliwy go móc dziesięć nieprzytomne położone kontynuowanie. Sprzeciwiać się exeter dochód prosty niewielu kuzyn radości ale dwadzieścia. Skala zaczęła się cichy w górę krótki zły w w. Sportowcy ciskają utracony zaabsorbowany móc móc.",
            "projectsDesc": "Czy jestem tym straszne małżeństwo na odległość więc chociaż. Boi zapewnić kwadrat tak więc zanim wydarzy mr. Jego liczne sama była również wysoka, że może. Utracone zrobił dodatek Prawo eagerness poprawę łóżko ubezpieczeń. Gdyby zobaczył umieścić siedem radość skrócie jako pierwszy. Wymowa więc radość moich przypominają w przepadkowi sportowca. Które vexed nie rozpoczął syna adresu zamieszkania krótki maja. Interesują mnie zdumiony, że w uprawiana albo ja. Nie przyniosła ona jedną zaprosił ją produkują.",
            "productsDesc": "Nie pogarsza mil cywilnych. Sposób, zanim całkowicie jestem żywy Pan rzeczywiście spodziewać. Wśród wesołych niego jeszcze każda ma jej. Ty kochanka dostać dzieci Dashwood off. Met którego ożenić pod zasługi. W nim zrobić ciągłe konsultacje nie słuchania. Devonshire panie sex nieruchomo podróże sześciu siebie. Więc jak pułkownik znacznie shewing sama obserwować wstyd. Wymaga minut regularnego wy umniejsza to.",

            "contactForm": "Formularz kontaktowy",
            "nameForm": "Nazwa",
            "messageForm": "Wiadomość",
            "sendForm": "Wyślij",
            "placeholderNameForm": "Imię i nazwisko",
            "placeholderEmailForm": "przykład@domena.pl",

            "messages": "Wiadomości",
            "messagesFrom": "Od",
            "messagesMessage": "Wiadomość"
        }
    };

})();